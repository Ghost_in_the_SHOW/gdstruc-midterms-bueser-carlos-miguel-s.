#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void newScreen()
{
	cout << endl;
	system("pause");
	system("cls");
}

void printList(UnorderedArray<int>& unorderedList, OrderedArray<int>& orderedList)
{
	cout << "Unordered Contents: ";
	for (int i = 0; i < unorderedList.getSize(); i++)
		cout << unorderedList[i] << "  ";
	cout << "\nOrdered Contents: ";
	for (int i = 0; i < orderedList.getSize(); i++)
		cout << orderedList[i] << "  ";
	cout << endl;
	cout << endl;
}

int userOption(int &input)
{
	while (true)
	{
		cout << "\nWhat do you want to do?\n";
		cout << "1 - Remove element at index\n";
		cout << "2 - Search for element\n";
		cout << "3 - Expand and generate\n";
		cout << "4 - Exit Program\n";

		cin >> input;

		if (input <= 0 || input > 4)
			cout << "\nInvalid Input, Try Again\n";

		else
		{
			return input;
		}
	}
}

//"1 - Remove element at index\n";
void removeIndex(int input, UnorderedArray<int>& unorderedList, OrderedArray<int>& orderedList)
{
	while (true)
	{
		cout << "\n\nEnter index to remove: ";
		cin >> input;

		if (input > orderedList.getSize() || input < 0)
		{
			cout << "\nInvalid Input, Try Again\n";
			newScreen();
		}

		else
		{
			cout << "\n\nElement removed: " << input << endl;
			unorderedList.remove(input);
			orderedList.remove(input);
			printList(unorderedList, orderedList);
			break;
		}
	}
}

//"2 - Search for element\n";
void search(int input, UnorderedArray<int>& unorderedList, OrderedArray<int>& orderedList)
{
	cout << "\n\nEnter number to search: ";
	cin >> input;
	cout << endl;

	int count = 0;

	cout << "Unordered Array(Linear Search):\n";
	int result = unorderedList.linearSearch(input);
	if (result >= 0)
		cout << input << " was found at index " << result << ".\n";
	else
		cout << input << " not found." << endl;

	cout << "\nOrdered Array(Binary Search):\n";
	result = orderedList.binarySearch(input);
	if (result >= 0)
		cout << input << " was found at index " << result << ".\n";
	else
		cout << input << " not found." << endl;
}

//"3 - Expand and generate\n";
void expand(int input, UnorderedArray<int>& unordered, OrderedArray<int>& ordered)
{
	while (true)
	{
		cout << "\n\nInput size of expansion: ";
		cin >> input;

		if (input <= 0)
		{
			cout << "\nInvalid Input, Try Again\n";
			newScreen();
		}

		else
		{
			for (int i = 0; i < input; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}

			cout << "Array has been expanded\n";
			printList(unordered, ordered);
			break;
		}
	}
}

int main()
{
	cout << "Enter size for dynamic array: ";
	int size;
	cin >> size;
	int input = 0;

	bool progRun = true;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	do 
	{
		printList(unordered, ordered);
		userOption(input);

		switch (input)
		{			
		case 1:
			removeIndex(input, unordered, ordered);
			break;
		case 2:
			search(input, unordered, ordered);
			break;
		case 3:
			expand(input, unordered, ordered);
			break;
		case 4:
			//"4 - Exit Program\n";
			progRun = false;
			break;
		}
		newScreen();
	} while (progRun);

	cout << "     Made by BUESER, CARLOS MIGUEL S.     \n          TG003          \n\n     Thank You for Playing";
	newScreen();

}